﻿using Delone;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace DeloneTest
{
    
    
    /// <summary>
    ///This is a test class for MainFormTest and is intended
    ///to contain all MainFormTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MainFormTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SortCounterClock
        ///</summary>
        [TestMethod()]
        [DeploymentItem( "Delone.exe" )]
        public void SortCounterClockTest()
        {
            MainForm_Accessor target = new MainForm_Accessor(); // TODO: Initialize to an appropriate value
            List<CPoint> pts = new List<CPoint> { new CPoint( 5, 1 ), new CPoint( 1, 2 ), new CPoint( 3, 4 ) }; // TODO: Initialize to an appropriate value
            List<CPoint> expected = new List<CPoint> { new CPoint( 1, 2 ), new CPoint( 3, 4 ), new CPoint( 5, 1 ) }; // TODO: Initialize to an appropriate value
            List<CPoint> actual = target.SortCounterClock( pts );

            Assert.IsTrue( expected[0] == actual[0] && expected[1] == actual[1] && expected[2] == actual[2] );//Assert.AreEqual( expected, actual );
        }
    }
}
