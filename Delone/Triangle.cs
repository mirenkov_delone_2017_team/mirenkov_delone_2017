﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Delone
{
    class Triangle
    {
        public float Circle_R { get; private set; }
        public CPointF Circle_Center { get; private set; }

        public Line line1 { get; private set; } 
        public Line line2 { get; private set; }
        public Line line3 { get; private set; }

        public CPoint point1 { get { return line1.point1; } set { line3.point2 = value; line1.point1 = value; RecalculateCircle(); } }
        public CPoint point2 { get { return line2.point1; } set { line1.point2 = value; line2.point1 = value; RecalculateCircle(); } }
        public CPoint point3 { get { return line3.point1; } set { line2.point2 = value; line3.point1 = value; RecalculateCircle(); } }

        public LineList Lines { get; private set; }

        public Triangle() { Circle_R = 0; Circle_Center = new CPointF(); line1 = new Line( 0, 0, 0, 0 ); line2 = new Line( 0, 0, 0, 0 ); line3 = new Line( 0, 0, 0, 0 ); Lines = new LineList( line1, line2, line3 ); }
        public Triangle( CPoint pt1, CPoint pt2, CPoint pt3 ) { Circle_R = 0; Circle_Center = new CPointF(); line1 = new Line( 0, 0, 0, 0 ); line2 = new Line( 0, 0, 0, 0 ); line3 = new Line( 0, 0, 0, 0 ); point1 = pt1; point2 = pt2; point3 = pt3; Lines = new LineList( line1, line2, line3 ); }
        public CPoint this[int index]
        {
            get { switch( index ) { case 1: return point2; case 2: return point3; default: return point1; } }
            set { switch( index ) { case 0: point1 = value; break; case 1: point2 = value; break; case 2: point3 = value; break; default: break; } }
        }

        /// <summary>
        /// Returns point of missing index
        /// </summary>
        /// <param name="i1">Index of 1st point of triagle</param>
        /// <param name="i2">Index of 2nd point of triagle</param>
        /// <returns>3rd point of triagle</returns>
        public CPoint this[int i1, int i2]
        {
            get { return this[( i1 + i2 - 3 ) * ( -1 )]; }
            set { this[( i1 + i2 - 3 ) * ( -1 )] = value; }
        }

        public override bool Equals( object obj )
        {
            if( ( base.Equals( null ) && obj != null ) || ( !base.Equals( null ) && obj == null ) ) return false;
            if( base.Equals( null ) && obj == null ) return true;
            int intersects = 0;
            for( int i = 0; i < 3; i++ )
                for( int j = 0; j < 3; j++ )
                    if( this[i] == ( (Triangle)obj )[j] )
                        intersects++;

            return (intersects == 3);
        }

        public void RecalculateCircle()
        {
            float x1 = point1.X;
            float y1 = point1.Y;
            float x2 = point2.X;
            float y2 = point2.Y;
            float x3 = point3.X;
            float y3 = point3.Y;

            float ma = ( y2 - y1 ) / ( x2 - x1 );
            float mb = ( y3 - y2 ) / ( x3 - x2 );

            Circle_Center.X = ( ma * mb * ( y1 - y3 ) + mb * ( x1 + x2 ) - ma * ( x2 + x3 ) ) / ( 2 * ( mb - ma ) );
            Circle_Center.Y = ( ( y1 + y2 ) / 2 ) - ( ( Circle_Center.X - ( ( x1 + x2 ) / 2 ) ) / ma );

            Circle_R = (float)CPointF.Distance( point1, Circle_Center );
        }

        public static bool Flip( Triangle tr1, Triangle tr2 )
        {
            int common_point1_tr1, common_point2_tr1, common_point1_tr2, common_point2_tr2;
            if( !HasCommonPointsIndexes( tr1, tr2, out common_point1_tr1, out common_point2_tr1, out common_point1_tr2, out common_point2_tr2 ) )
                return false;

            if( !IsConvex( tr1, tr2 ) )
                return false;

            int iouter_point_tr1 = ( common_point1_tr1 + common_point2_tr1 + 1 ) % 2;
            int iouter_point_tr2 = ( common_point1_tr2 + common_point2_tr2 + 1 ) % 2;
            CPoint outer_point_tr1 = tr1[common_point1_tr1, common_point2_tr1];
            CPoint outer_point_tr2 = tr2[common_point1_tr2, common_point2_tr2];

            tr2[common_point2_tr2] = outer_point_tr1;
            tr1[common_point1_tr1] = outer_point_tr2;


            return true;
        }

        public bool ContainPoint( CPoint p ) { return ContainPoint( this, p ); }
        public static bool ContainPoint( Triangle tr, CPoint p )
        {
            int pt1 = ( ( tr.point1.X - p.X ) * ( tr.point2.Y - tr.point1.Y ) ) - ( ( tr.point2.X - tr.point1.X ) * ( tr.point1.Y - p.Y ) );
            int pt2 = ( ( tr.point2.X - p.X ) * ( tr.point3.Y - tr.point2.Y ) ) - ( ( tr.point3.X - tr.point2.X ) * ( tr.point2.Y - p.Y ) );
            int pt3 = ( ( tr.point3.X - p.X ) * ( tr.point1.Y - tr.point3.Y ) ) - ( ( tr.point1.X - tr.point3.X ) * ( tr.point3.Y - p.Y ) );

            return ( pt1 > 0 && pt2 > 0 && pt3 > 0 ) || ( pt1 < 0 && pt2 < 0 && pt3 < 0 );
        }

        public static bool HasCommonPointsIndexes( Triangle tr1, Triangle tr2,
            out int common_point1_tr1, out int common_point2_tr1, out int common_point1_tr2, out int common_point2_tr2 )
        {
            common_point1_tr1 = -1;
            common_point2_tr1 = -1; 
            common_point1_tr2 = -1; 
            common_point2_tr2 = -1;

            for( int i = 0; i < 3; i++ )
                for( int j = 0; j < 3; j++ )
                    if( tr1[i] == tr2[j] )
                    {
                        if( common_point1_tr1 == -1 )
                        {
                            common_point1_tr1 = i;
                            common_point1_tr2 = j;
                        }
                        else
                        {
                            common_point2_tr1 = i;
                            common_point2_tr2 = j;
                        }
                    }

            return ( common_point1_tr1 != -1 && common_point1_tr2 != -1 && common_point2_tr1 != -1 && common_point2_tr2 != -1 );
        }

        public static bool IsConvex( Triangle tr1, Triangle tr2 )
        {
            int common_point1_tr1, common_point2_tr1, common_point1_tr2, common_point2_tr2;
            if( HasCommonPointsIndexes( tr1, tr2, out common_point1_tr1, out common_point2_tr1, out common_point1_tr2, out common_point2_tr2 ) )
            {
                CPoint A = (Point)tr1[common_point1_tr1, common_point2_tr1];
                CPoint B = (Point)tr1[common_point1_tr1];
                CPoint C = (Point)tr2[common_point1_tr2, common_point2_tr2];
                CPoint D = (Point)tr1[common_point2_tr1];

                int AB_BC = CPoint.VectorProduct( B - A, C - B );
                int BC_CD = CPoint.VectorProduct( C - B, D - C );
                int CD_DA = CPoint.VectorProduct( D - C, A - D );
                int DA_AB = CPoint.VectorProduct( A - D, B - A );

                return ( ( AB_BC > 0 && BC_CD > 0 && CD_DA > 0 && DA_AB > 0 ) || ( AB_BC < 0 && BC_CD < 0 && CD_DA < 0 && DA_AB < 0 ) );
            }

            return false;
        }
    }
}
