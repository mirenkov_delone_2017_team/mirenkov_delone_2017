﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Delone
{
    public class CPoint
    {
        public int X { get; set; }
        public int Y { get; set; }
        public CPoint() { }
        public CPoint( int x, int y ) { this.X = x; this.Y = y; }

        public static implicit operator Point( CPoint p ) { return new Point( p.X, p.Y ); }
        public static implicit operator CPoint( Point p ) { return new CPoint( p.X, p.Y ); }

        public static CPoint operator+( CPoint pt1, CPoint pt2 ) { return new CPoint( pt1.X + pt2.X, pt1.Y + pt2.Y ); }
        public static CPoint operator-( CPoint pt1, CPoint pt2 ) { return new CPoint( pt1.X - pt2.X, pt1.Y - pt2.Y ); }

        public static bool operator ==( CPoint pt1, CPoint pt2 ) { return pt1.Equals( pt2 ); }
        public static bool operator !=( CPoint pt1, CPoint pt2 ) { return !( pt1 == pt2 ); }

        public override bool Equals( object obj )
        {
            if( obj == null ) return false;
            CPoint outer = (CPoint)obj;

            return ( outer.X == this.X && outer.Y == this.Y );
        }

        public override string ToString()
        {
            return "(" + X.ToString() + ", " + Y.ToString() + ")";
        }

        public static int VectorProduct( CPoint a, CPoint b )
        {
            return ( a.X * b.Y ) - ( a.Y * b.X );
        }

        public static double Distance( CPoint a, CPoint b )
        {
            return Math.Sqrt( Math.Pow( b.X - a.X, 2 ) + Math.Pow( b.Y - a.Y, 2 ) );
        }
    }

    public class CPointF
    { 
        public float X { get; set; }
        public float Y { get; set; }
        public CPointF() { }
        public CPointF( float x, float y ) { this.X = x; this.Y = y; }

        public static implicit operator CPoint( CPointF p )
        {
            return new CPoint( Convert.ToInt32( p.X ), Convert.ToInt32( p.Y ) );
        }

        public static implicit operator CPointF( CPoint p )
        {
            return new CPointF( p.X, p.Y );
        }

        public static double Distance( CPointF a, CPointF b )
        {
            return Math.Sqrt( Math.Pow( b.X - a.X, 2 ) + Math.Pow( b.Y - a.Y, 2 ) );
        }
    }
}
