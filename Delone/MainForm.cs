﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Delone
{
    public partial class MainForm : Form
    {
        Brush PointsColor = new SolidBrush( Color.Black );
        Pen LinesColor = new Pen( Color.Green );
        Pen CircleColor = new Pen( Color.Blue );
        CPoint PointsSize = new CPoint( 2, 2 );
        
        List<CPoint> Points = new List<CPoint>();
        LineList Lines = new LineList();
        List<Triangle> Triangles = new List<Triangle> ();
        bool DrawCircles = false;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Paint( object sender, PaintEventArgs e )
        {
            Graphics graphics = e.Graphics;

            for( int i = 0; i < Triangles.Count; i++ )
            {
                graphics.DrawLine( ( Triangles[i].line1.debugcolor == null ) ? LinesColor : Triangles[i].line1.debugcolor, Triangles[i].line1.point1, Triangles[i].line1.point2 );
                graphics.DrawLine( ( Triangles[i].line1.debugcolor == null ) ? LinesColor : Triangles[i].line2.debugcolor, Triangles[i].line2.point1, Triangles[i].line2.point2 );
                graphics.DrawLine( ( Triangles[i].line1.debugcolor == null ) ? LinesColor : Triangles[i].line3.debugcolor, Triangles[i].line3.point1, Triangles[i].line3.point2 );

                if( DrawCircles )
                    graphics.DrawEllipse( CircleColor, ( Triangles[i].Circle_Center.X - Triangles[i].Circle_R ), ( Triangles[i].Circle_Center.Y - Triangles[i].Circle_R ), Triangles[i].Circle_R * 2, Triangles[i].Circle_R * 2 );

                //graphics.FillRectangle( PointsColor, Triangles[i].point1.X - ( PointsSize.X / 2 ), Triangles[i].point1.Y - ( PointsSize.Y / 2 ), PointsSize.X, PointsSize.Y );
                //graphics.FillRectangle( PointsColor, Triangles[i].point2.X - ( PointsSize.X / 2 ), Triangles[i].point2.Y - ( PointsSize.Y / 2 ), PointsSize.X, PointsSize.Y );
                //graphics.FillRectangle( PointsColor, Triangles[i].point3.X - ( PointsSize.X / 2 ), Triangles[i].point3.Y - ( PointsSize.Y / 2 ), PointsSize.X, PointsSize.Y );
            }

            for( int i = 0; i < Points.Count; i++ )
                graphics.FillRectangle( PointsColor, Points[i].X - ( PointsSize.X / 2 ), Points[i].Y - ( PointsSize.Y / 2 ), PointsSize.X, PointsSize.Y );
        }

        private void MainForm_MouseUp( object sender, MouseEventArgs e )
        {
            CPoint NewCPoint = new CPoint( e.X, e.Y );
            if( !Points.Contains( NewCPoint ) )
            {
                AddPoint( NewCPoint );
                Refresh();
            }
        }

        private void bt_Clear_Click( object sender, EventArgs e )
        {
            Points.Clear();
            Lines.Clear();
            Triangles.Clear();
            Refresh();
        }

        private void AddPoint( CPoint point )
        {
            if( point.Y > 47 && !Points.Exists( x => ( Math.Abs( x.X - point.X ) < 5 || Math.Abs( x.Y - point.Y ) < 5 ) )  )
                Points.Add( point );
        }

        private void bt_Triangulate_Click( object sender, EventArgs e )
        {
            Triangulate();
            Refresh();
        }

        private void bt_Delone_Click( object sender, EventArgs e )
        {
            FixDelone();
            Refresh();
        }

        private void bt_Circles_Click( object sender, EventArgs e )
        {
            DrawCircles = !DrawCircles;
            Refresh();
        }

        private void lb_colorbar_Click( object sender, EventArgs e )
        {
            Random rand = new Random();
            lb_colorbar.BackColor = Color.FromArgb( rand.Next( 0, 255 ), rand.Next( 0, 255 ), rand.Next( 0, 255 ) );
        }
    }
}
