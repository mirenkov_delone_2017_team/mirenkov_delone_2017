﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Delone
{
    public class Matrix<T> : List<List<double>>
    {
        public Matrix()
        {
        }

        public Matrix( T input )
        {
            Console.WriteLine( input.GetType().ToString() );
        }

        public void TypeIt( T input ) { Console.WriteLine( input.GetType().ToString() ); }

        public double CalcDet()
        {
            return CalcDet( this );
        }

        public static double CalcDet2x2( Matrix<T> matrix )
        {
            return ( matrix[0][0] * ( matrix[1][1] ) - ( matrix[1][0] * matrix[0][1] ) );
        }

        public static double CalcDet( Matrix<T> matrix )
        {
            if( matrix.Count == 2 )
                return CalcDet2x2( matrix );

            double result = 0;
            for( int i = 0; i < matrix.Count; i++ )
                result += ( ( i % 2 == 0 ) ? -1 : 1 ) * ( matrix[i][0] ) * CalcDet( GetSubmatrix( matrix, i, 0 ) );

            return result;
        }

        public Matrix<T> GetSubmatrix( int left, int top, int right, int bottom )
        {
            return GetSubmatrix( this, left, top, right, bottom );
        }

        public static Matrix<T> GetSubmatrix( Matrix<T> matrix, int left, int top, int right, int bottom )
        {
            Matrix<T> result = new Matrix<T>();

            int height = bottom - top;
            int width = right - left;

            for( int i = 0; i < width; i++ )
            {
                result.Add( new List<double>() );
                for( int j = 0; j < height; j++ )
                    result[i].Add( matrix[left + i][top + j] );
            }

            return result;
        }

        public Matrix<T> GetSubmatrix( int minus_row, int minus_column ) { return GetSubmatrix( this, minus_row, minus_column ); }
        public static Matrix<T> GetSubmatrix( Matrix<T> matrix, int minus_row, int minus_column )
        {
            Matrix<T> result = new Matrix<T>();

            for( int i = 0; i < matrix.Count; i++ )
            {
                if( i == minus_row )
                    continue;

                result.Add( new List<double>() );

                for( int j = 0; j < matrix[i].Count; j++ )
                {
                    if( j == minus_column )
                        continue;

                    result[result.Count - 1].Add( matrix[i][j] );
                }
            }
            return result;
        }

        public Matrix<T> GetCopy() { return GetCopy( this ); }

        public static Matrix<T> GetCopy( Matrix<T> matrix )
        {
            Matrix<T> new_matrix = new Matrix<T>();

            for( int i = 0; i < matrix.Count; i++ )
            {
                new_matrix.Add( new List<double>() );
                for( int j = 0; j < matrix[i].Count; j++ )
                {
                    new_matrix[i].Add( matrix[i][j] );
                }
            }
            return new_matrix;
        }
    }
}
