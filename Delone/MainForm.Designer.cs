﻿namespace Delone
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_Triangulate = new System.Windows.Forms.Button();
            this.bt_Clear = new System.Windows.Forms.Button();
            this.lb_control_panel = new System.Windows.Forms.Label();
            this.bt_Circles = new System.Windows.Forms.Button();
            this.lb_delone = new System.Windows.Forms.Label();
            this.lb_colorbar = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bt_Triangulate
            // 
            this.bt_Triangulate.Location = new System.Drawing.Point( 12, 12 );
            this.bt_Triangulate.Name = "bt_Triangulate";
            this.bt_Triangulate.Size = new System.Drawing.Size( 75, 23 );
            this.bt_Triangulate.TabIndex = 0;
            this.bt_Triangulate.Text = "triangulate";
            this.bt_Triangulate.UseVisualStyleBackColor = true;
            this.bt_Triangulate.Click += new System.EventHandler( this.bt_Triangulate_Click );
            // 
            // bt_Clear
            // 
            this.bt_Clear.Location = new System.Drawing.Point( 93, 12 );
            this.bt_Clear.Name = "bt_Clear";
            this.bt_Clear.Size = new System.Drawing.Size( 75, 23 );
            this.bt_Clear.TabIndex = 1;
            this.bt_Clear.Text = "clear";
            this.bt_Clear.UseVisualStyleBackColor = true;
            this.bt_Clear.Click += new System.EventHandler( this.bt_Clear_Click );
            // 
            // lb_control_panel
            // 
            this.lb_control_panel.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
                        | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.lb_control_panel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lb_control_panel.Location = new System.Drawing.Point( 0, 0 );
            this.lb_control_panel.Name = "lb_control_panel";
            this.lb_control_panel.Size = new System.Drawing.Size( 260, 47 );
            this.lb_control_panel.TabIndex = 2;
            // 
            // bt_Circles
            // 
            this.bt_Circles.Location = new System.Drawing.Point( 174, 12 );
            this.bt_Circles.Name = "bt_Circles";
            this.bt_Circles.Size = new System.Drawing.Size( 75, 23 );
            this.bt_Circles.TabIndex = 8;
            this.bt_Circles.Text = "circles";
            this.bt_Circles.UseVisualStyleBackColor = true;
            this.bt_Circles.Click += new System.EventHandler( this.bt_Circles_Click );
            // 
            // lb_delone
            // 
            this.lb_delone.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.lb_delone.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lb_delone.Location = new System.Drawing.Point( 232, -1 );
            this.lb_delone.Name = "lb_delone";
            this.lb_delone.Size = new System.Drawing.Size( 25, 13 );
            this.lb_delone.TabIndex = 9;
            this.lb_delone.Click += new System.EventHandler( this.bt_Delone_Click );
            // 
            // lb_colorbar
            // 
            this.lb_colorbar.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
                        | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.lb_colorbar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lb_colorbar.Location = new System.Drawing.Point( 2, 2 );
            this.lb_colorbar.Name = "lb_colorbar";
            this.lb_colorbar.Size = new System.Drawing.Size( 255, 8 );
            this.lb_colorbar.TabIndex = 10;
            this.lb_colorbar.Click += new System.EventHandler( this.lb_colorbar_Click );
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 259, 262 );
            this.Controls.Add( this.lb_colorbar );
            this.Controls.Add( this.lb_delone );
            this.Controls.Add( this.bt_Circles );
            this.Controls.Add( this.bt_Clear );
            this.Controls.Add( this.bt_Triangulate );
            this.Controls.Add( this.lb_control_panel );
            this.Name = "MainForm";
            this.Text = "Delone";
            this.Paint += new System.Windows.Forms.PaintEventHandler( this.MainForm_Paint );
            this.MouseUp += new System.Windows.Forms.MouseEventHandler( this.MainForm_MouseUp );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.Button bt_Triangulate;
        private System.Windows.Forms.Button bt_Clear;
        private System.Windows.Forms.Label lb_control_panel;
        private System.Windows.Forms.Button bt_Circles;
        private System.Windows.Forms.Label lb_delone;
        private System.Windows.Forms.Label lb_colorbar;


    }
}

