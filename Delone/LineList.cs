﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Delone
{
    public class Line
    {
        public bool Marked = false;
        public Pen debugcolor;
        public Pen DebugColor { get { return debugcolor; } set{ if( debugcolor != null ) debugcolor.Dispose(); debugcolor = value; } }  // = new Pen( Color.Black );
        public Line() { DebugColor = new Pen( Color.Green ); }
        public Line( CPoint pt1, CPoint pt2 ) : this() { point1 = pt1; point2 = pt2; }
        public Line( int x1, int y1, int x2, int y2 ) { point1 = new CPoint( x1, y1 ); point2 = new CPoint( x2, y2 ); }
        public CPoint point1;
        public CPoint point2;
        public override bool Equals( object obj )
        {
            if( ( base.Equals( null ) && obj != null ) || ( !base.Equals( null ) && obj == null ) ) return false;
            if( base.Equals( null ) && obj == null ) return true;
            return ( this.point1 == ( (Line)obj ).point1 && this.point2 == ( (Line)obj ).point2 ) || ( this.point1 == ( (Line)obj ).point2 && this.point2 == ( (Line)obj ).point1 ); 
        }

        public override int  GetHashCode() { return base.GetHashCode(); }
        public override string ToString() { return "(" + point1.X.ToString() + ", " + point1.Y.ToString() + ") (" + point2.X.ToString() + ", " + point2.Y.ToString() + ")"; }
        public static bool Intersect( Line l1, Line l2 ) 
        {
            CPoint start1 = l1.point1;
            CPoint end1 = l1.point2;
            CPoint start2 = l2.point1;
            CPoint end2 = l2.point2;

            CPoint dir1 = end1 - start1;
            CPoint dir2 = end2 - start2;

            //считаем уравнения прямых проходящих через отрезки
            float a1 = -dir1.Y;
            float b1 = +dir1.X;
            float d1 = -( a1 * start1.X + b1 * start1.Y );

            float a2 = -dir2.Y;
            float b2 = +dir2.X;
            float d2 = -( a2 * start2.X + b2 * start2.Y );

            //подставляем концы отрезков, для выяснения в каких полуплоскотях они
            float seg1_line2_start = a2 * start1.X + b2 * start1.Y + d2;
            float seg1_line2_end = a2 * end1.X + b2 * end1.Y + d2;

            float seg2_line1_start = a1 * start2.X + b1 * start2.Y + d1;
            float seg2_line1_end = a1 * end2.X + b1 * end2.Y + d1;

            //если концы одного отрезка имеют один знак, значит он в одной полуплоскости и пересечения нет.
            if( seg1_line2_start * seg1_line2_end >= 0 || seg2_line1_start * seg2_line1_end >= 0 )
                return false;

            float u = seg1_line2_start / ( seg1_line2_start - seg1_line2_end );
            //CPoint out_intersection = start1 + u * dir1;

            return true;
        }
    }

    class LineList : List<Line>
    {
        public LineList() { }
        public LineList( params Line[] list ) { this.AddRange( list ); }

        public Line this[CPoint pt1, CPoint pt2]
        {
            get { return this.Find( x => ( x.point1 == pt1 && x.point2 == pt2 ) || ( x.point1 == pt2 && x.point2 == pt1 ) ); } 
            set
            {
                Line line = this[pt1, pt2];//.Find( x => x == new Line( pt1, pt2 ) );
                if( line != null ) this[IndexOf( line )] = value;
            }
        }

        public List<Line> this[CPoint pt] { get { return this.FindAll( x => x.point1 == pt || x.point2 == pt ); } }

        public Line Remove( CPoint pt1, CPoint pt2 )
        { 
            Line line = this[pt1, pt2];
            this.Remove( line );
            return line;
        }

        public new bool Add( Line item )
        {
            if( Contains( item ) )
            {
                LogList.Log += "line " + item.ToString() + " already exist\n";
                return false;
            }
            else
            {
                base.Add( item );
                return true;
            }
        }

        public bool Add( CPoint pt1, CPoint pt2 )
        {
            Line line = new Line( pt1, pt2 );
            if( Contains( line ) )
            {
                LogList.Log += "line " + line.ToString() + " already exist\n";
                return false;
            }
            else
            {
                base.Add( line );
                return true;
            }
        }

        public bool AddIntersectionless( CPoint pt1, CPoint pt2 )
        {
            Line line = new Line( pt1, pt2 );

            if( Contains( line ) )
            {
                LogList.Log += "line " + line.ToString() + " already exist\n";
                return false;
            }

            for( int i = 0; i < this.Count; i++ )
            { 
                if( Line.Intersect( this[i], line ) )
                    return false;
            }

            base.Add( line );
            return true;
        }
    }
}
