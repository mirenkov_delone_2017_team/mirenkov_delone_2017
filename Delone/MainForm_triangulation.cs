﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Delone
{
    public partial class MainForm : Form
    {
        void Triangulate()
        {
            RandomTriangulate();
            for( int i = 0; i < Triangles.Count; i++ )
                FixDelone();
        }

        void RandomTriangulate()
        {
            for( int i = 0; i < Points.Count; i++ )
            {
                for( int j = 0; j < Points.Count; j++ )
                {
                    if( i == j )
                        continue;

                    Lines.AddIntersectionless( Points[i], Points[j] );
                }
            }

            for( int i = 0; i < Points.Count; i++ )
            {
                for( int j = i + 1; j < Points.Count; j++ )
                {
                    if( Lines[Points[i], Points[j]] != null )
                    {
                        for( int k = j + 1; k < Points.Count; k++ )
                        {
                            if( Lines[Points[j], Points[k]] != null && Lines[Points[k], Points[i]] != null )
                            {
                                Triangle NewTriangle = new Triangle( Points[i], Points[j], Points[k] );
                                bool contain = false;

                                for( int p = 0; p < Points.Count; p++ )
                                    if( NewTriangle.ContainPoint( Points[p] ))
                                        contain = true;

                                if( !contain )
                                    Triangles.Add( NewTriangle );
                            }
                        }
                    }
                }
            }
        }


        int flip_success = 0;
        int flip_fail = 0;

        void FixDelone()
        {
            flip_success = 0;
            flip_fail = 0;

            for( int i = 0; i < Triangles.Count; i++ )
            {
                for( int j = i + 1; j < Triangles.Count; j++ )
                {
                    for( int k = 0; k < 3; k++ )
                    {
                        if( ContainsPointInCircle( Triangles[i].point1, Triangles[i].point2, Triangles[i].point3, Triangles[j][k] ) &&
                            Triangles[i].point1 != Triangles[j][k] && Triangles[i].point2 != Triangles[j][k] && Triangles[i].point3 != Triangles[j][k] )
                        {
                            if( Triangle.Flip( Triangles[i], Triangles[j] ) )
                                flip_success++;
                            else
                                flip_fail++;
                            break;
                        }
                    }
                }
            }

            //lb_control_panel.Text = flip_success.ToString() + " " + flip_fail.ToString();
        }
        
        void swap( ref int a, ref int b ) { int temp = a; a = b; b = temp; }

        List<CPoint> SortCounterClock( List<CPoint> pts )
        {
            if( pts.Count != 3 )
                return pts;

            List<CPoint> result = new List<CPoint>();

            CPoint first = pts.OrderBy( x => x.X ).ToList()[0];
            result.Add( first );
            pts.Remove( first );
            result.AddRange( pts.OrderByDescending( x => x.Y ) );

            return result;
        }

        bool ContainsPointInCircle( CPoint p1, CPoint p2, CPoint p3, CPoint tested_point )
        {
            List<CPoint> pts = SortCounterClock( new List<CPoint> { p1, p2, p3 } );

            Matrix<double> matrix = new Matrix<double> 
            {  
            new List<double>{ pts[0].X, pts[0].Y, Math.Pow( pts[0].X, 2 ) + Math.Pow( pts[0].Y, 2 ), 1 },
            new List<double>{ pts[1].X, pts[1].Y, Math.Pow( pts[1].X, 2 ) + Math.Pow( pts[1].Y, 2 ), 1 },
            new List<double>{ pts[2].X, pts[0].Y, Math.Pow( pts[2].X, 2 ) + Math.Pow( pts[2].Y, 2 ), 1 },
            new List<double>{ tested_point.X, tested_point.Y, Math.Pow( tested_point.X, 2 ) + Math.Pow( tested_point.Y, 2 ), 1 },
            };

            double det = Matrix<double>.CalcDet( matrix );

            return (det > 0);
        }
    }
}